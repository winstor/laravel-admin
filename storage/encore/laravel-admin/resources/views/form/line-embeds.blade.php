<div class="{{$viewClass['form-group']}}">
  <label class="{{$viewClass['label']}} control-label">{{$label}}</label>
<div class="{{$viewClass['field']}}">
    <div class="embed-{{$column}}-forms">
        <div class="embed-{{$column}}-form fields-group">
            <table class="table embed embed-{{$column}}" style="margin-bottom:0;">
                <thead>
                <tr>
                    @foreach($headers as $header)
                        <th>{!! $header !!}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody class="has-many-{{$column}}-forms">
                    <tr class="has-many-{{$column}}-form fields-group">
                        @foreach($form->fields() as $field)
                            <td>{!! $field->setLabelClass(['hidden'])->setWidth(12, 0)->render() !!}</td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
