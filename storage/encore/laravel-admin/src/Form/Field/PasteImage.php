<?php


namespace Encore\Admin\Form\Field;


use Encore\Admin\Form\Field;

class PasteImage extends Field
{
    /**
     * Htmlable.
     *
     * @var string|\Closure
     */
    protected $html = '';

    /**
     * @var string
     */
    protected $label = '';

    /**
     * Render html field.
     *
     * @return string
     */
    public function render()
    {
        $this->setPasteStyle();
        $this->setupPasteScript();
        $viewClass = $this->getViewElementClasses();
        return <<<EOT
<div class="{$viewClass['form-group']}">
    <label  class="{$viewClass['label']} control-label">{$this->label}</label>
    <div class="{$viewClass['field']}">
        <div class="from_clipboard" id="paste_{$this->column}" contenteditable="true">
        </div>
        <span class="help-block">
            <i class="fa fa-info-circle"></i>&nbsp;粘贴板&nbsp;&nbsp;
            <span style="width: 100px">
            <a id="paste_delete_{$this->column}" href="javascript:void(0);" style="">&nbsp;<i class="fa fa-trash"></i>&nbsp;删除</a>
            </span>
        </span>
        <input id="{$this->column}" name="{$this->column}" type="hidden" value="">
    </div>
</div>
EOT;
    }

    protected function setPasteStyle()
    {
        $style = <<<STYLE
.from_clipboard{
    width: 100%;
    max-height: 300px;
    min-height: 100px;
    border: 1px solid;
    overflow-x: hidden;
}

.from_clipboard img{
    max-width:100%;
}
STYLE;
        \Encore\Admin\Admin::style($style);
    }

    protected function setupPasteScript()
    {
        $script = <<<SCRIPT

document.getElementById('paste_{$this->column}').addEventListener('paste', function (e) {
    //e.preventDefault();
    document.getElementById('paste_{$this->column}').innerHTML = '';
    let paste_that = document.getElementById('paste_{$this->column}');
    let item = e.clipboardData.items[0]
    if (item.kind === "string") {
        item.getAsString(function (str) {
            document.getElementById('paste_{$this->column}').innerHTML = '';
        })
    }else if (item.kind === "file") {
        let paste_reader = new FileReader();
        paste_reader.readAsDataURL(item.getAsFile());
        paste_reader.onload = function (event) {
             document.getElementById('{$this->column}').value = event.target.result
             document.getElementById('paste_delete_{$this->column}').style.cssText = '';
        }
    }
});
document.getElementById('paste_delete_{$this->column}').onclick = function(){
    document.getElementById('paste_{$this->column}').innerHTML = '';
    document.getElementById('{$this->column}').value = '';
    //document.getElementById('paste_delete_{$this->column}').style.cssText = 'display: none';
}

document.getElementById('paste_{$this->column}').addEventListener("input", function(e) {
    //deleteContentBackward 后退
    if(e.inputType !== 'insertFromPaste'){
        document.getElementById('paste_{$this->column}').innerHTML = '';
        document.getElementById('{$this->column}').value = '';
        //document.getElementById('paste_delete_{$this->column}').style.cssText = 'display: none';
    }
});

SCRIPT;
        \Encore\Admin\Admin::script($script);
    }

}
