<?php


namespace Encore\Admin\Grid\Displayers;

class Star extends AbstractDisplayer
{
    public function display($color = null)
    {
        $value = intval($this->value);
        return sprintf('<span title="%s星">%s</span>',
            $value,
            str_repeat($this->star($color), $value)
        );
    }

    protected function star($color)
    {
        $color = $color ?: '#ff8913';
        return sprintf('<i class="fa fa-star" style="color:%s"></i>', $color);
    }
}
