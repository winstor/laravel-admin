<?php


namespace Encore\Admin\Grid\Displayers;s;

use Carbon\Carbon;

class DateFormat extends AbstractDisplayer
{
    public function display($format = 'Y-m-d')
    {
        if ($value = $this->value) {
            return Carbon::parse($value)->format($format);
        }
        return '';
    }
}
